# Terraform EMBL example

The following repository serves as an example of a minimal `terraform + ansible` recipe that creates a one machine setup.

`terraform` is responsible for creating the virtual machine in openstack, along with the required network components for HTTP, HTTPS and SSH access from outside EMBL.
`ansible` is used to further customize the created virtual machine. Currently this step provides little more than a "Hello World" but can be extended as necessary.

## Configuring terraform

A few bits of information are necessary to configure the creation process used by terraform.

### Generating Application Credentials

To begin with, you need to generate [Application Credentials](https://docs.openstack.org/keystone/queens/user/application_credentials.html) that will be used by terraform to authenticate with OpenStack.
Application Credentials are tokens that can be used as an alternative to logging in with your `username` and `password` and that can easily be replaced or re-generated.

On EMBL's de.NBI OpenStack cloud visit the [Application Credentials](https://denbi.cloud.embl.de/dashboard/identity/application_credentials/) interface and create a new application credential providing only a name.
You will then be presented with an `ID` and a `Secret` which you should use to configure terraform.

Variables can be passed to terraform directly in the command-line or through environment variables.  
For convenience we'll be using environment variables.

### SSH Authentication

As a final step in the process of creating a VM, terraform will connect to it via SSH and, once successful, will start ansible, that will also connect via SSH.  
For this process to work, you should have an `ssh-agent` running and `ssh-add` the private key that matches the public key specified in then `SSH_KEY` variable (see below for details).

Both terraform and ansible will automatically use the ssh-agent to authenticate.  

If this process doesn't work for you, make sure your `ssh-agent` doesn't have too many identities loaded.
Consider retrying after deleting all identities (`ssh-add -D`) and `ssh-add` the specific private key.

### Required variables

In addition to application credentials, the following variables need to be defined for terraform.
Variables that require a `{ key = value }` syntax indicate which keys need to be defined in `{}`.

* `OS_AUTH_URL` - the URL against which terraform will authenticate
* `APPLICATION_CREDENTIAL_ID` - the `ID` part of Application Credential described above
* `APPLICATION_CREDENTIAL_SECRET` - the `SECRET` part of Application Credential described above
* `IMAGE_ID` - `{ name, flavor_name, image_uuid, username }` - specify details about which operating system should be used as base of the virtual machine and how many resources should be used by indicating a flavor
* `NETWORK` - `{ name, cidr, ip_version, router_ip, external_network_uuid }` - define an internal network to which the virtual machine be connected. This is required to allow external access to the virtual machine.
* `FLOATING_IP` - the external address that will be used to access the virtual machine
* `SSH_KEY` - `{ name, public_key }` - the SSH public key for the first user on the system

optionally you may want to include:

* `OS_PROJECT_ID` - ID of the project where the virtual machine should be created
* `OS_REGION_NAME` - With OpenStack installations that span multiple locations, this variable defines which location should the image be launched at.

### Environment variables

When defining the above variables in the environment, all but those starting with `OS_` should be prefixed with `TF_VAR_`.

For example, `APPLICATION_CREDENTIAL_ID` becomes `TF_VAR_APPLICATION_CREDENTIAL_ID`.
Together with variables that require a `{ key = value }` syntax, you should export the variables before executing terraform.

```
export OS_AUTH_URL='https://denbi.cloud.embl.de:13000'
export TF_VAR_APPLICATION_CREDENTIAL_ID='7bf3b01d50dd4e50b12357c806b8b5a4'
export TF_VAR_APPLICATION_CREDENTIAL_SECRET='6tkkaq7s9h9Gz3b9Xk5UK-n2VodaElnwMLxdT7-L-DGGHy3r3rnc0lDjRA979saWQ55Xr-oaUiov7KlIXlR27g'
export TF_VAR_IMAGE_ID='{ name = "sandbox", flavor_name = "f1.optimal2", image_uuid = "de6f719b-1d64-4539-9826-e117254781e8", username = "centos" }'
export TF_VAR_NETWORK='{ name = "bio-it-net", cidr = "192.168.1.0/24", router_ip = "192.168.1.254", ip_version = 4, external_network_uuid = "b49ecf1c-37ab-4296-8c46-09e28b810d7f" }'
export TF_VAR_FLOATING_IP='193.175.249.99'
export TF_VAR_SSH_KEY='{ name = "Bio-IT-EMBL", public_key = "ssh-rsa AAAA(... replace this ...)pi0=" }'
```


### Pass syntax

The `terraform` wrapper script included in the repository uses [`pass`](https://www.passwordstore.org/) to store the environment configuration in a secure and convenient manner.

To setup `pass` in your computer, you can execute:

```
pass edit web/denbi.cloud.embl.de/terraform
```

and include the content below, modifying the variables where applicable.
Note that contrary to the statements above, no `export` is included here and values should not be quoted.

```
OS_AUTH_URL=https://denbi.cloud.embl.de:13000
TF_VAR_APPLICATION_CREDENTIAL_ID=7bf3b01d50dd4e50b12357c806b8b5a4
TF_VAR_APPLICATION_CREDENTIAL_SECRET=6tkkaq7s9h9Gz3b9Xk5UK-n2VodaElnwMLxdT7-L-DGGHy3r3rnc0lDjRA979saWQ55Xr-oaUiov7KlIXlR27g
TF_VAR_IMAGE_ID={ name = "sandbox", flavor_name = "f1.optimal2", image_uuid = "de6f719b-1d64-4539-9826-e117254781e8", username = "centos" }
TF_VAR_NETWORK={ name = "bio-it-net", cidr = "192.168.1.0/24", router_ip = "192.168.1.254", ip_version = 4, external_network_uuid = "b49ecf1c-37ab-4296-8c46-09e28b810d7f" }
TF_VAR_FLOATING_IP=193.175.249.99
TF_VAR_SSH_KEY={ name = "Bio-IT-EMBL", public_key = "ssh-rsa AAAA(... replace this ...)pi0=" }
```

## Ready, Set, Go

To create the virtual machine and associated infrastructure:

```
./terraform apply
```

to revert to a clean slate:

```
./terraform destroy
```

### Ansible

Ansible instructions are stored in `ansible/instance.yaml` and are executed by terraform once the virtual machine is setup and an IP address is assigned.

The template recipe is very simple and doesn't currently implement any variables or sophisticated logic.
