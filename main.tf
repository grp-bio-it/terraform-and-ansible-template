terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.43.0"
    }
  }
}

locals {
  ansible_section_header = "${var.IMAGE_ID.name}"
  ansible_user = "${var.IMAGE_ID.username}"
}

# Configure the OpenStack Provider - settings read from environment
provider "openstack" {
  application_credential_id     = var.APPLICATION_CREDENTIAL_ID
  application_credential_secret = var.APPLICATION_CREDENTIAL_SECRET
}

resource "openstack_networking_secgroup_v2" "secgroup_ssh" {
  name        = "SSH"
  description = "Allow inbound SSH"
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_ssh.id}"
}

resource "openstack_networking_secgroup_v2" "secgroup_http_https" {
  name        = "HTTP/HTTPS"
  description = "Allow inbound HTTP and HTTPS"
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_http_https.id}"
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_http_https.id}"
}

resource "openstack_compute_keypair_v2" "default_keypair_1" {
  # matching ~/.ssh/EMBL.pub
  name       = var.SSH_KEY.name
  public_key = var.SSH_KEY.public_key
}

resource "openstack_networking_network_v2" "network_1" {
  name           = var.NETWORK.name
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_1" {
  name       = "subnet_1"
  network_id = "${openstack_networking_network_v2.network_1.id}"
  cidr       = var.NETWORK.cidr
  ip_version = var.NETWORK.ip_version
  gateway_ip = var.NETWORK.router_ip
}

resource "openstack_networking_router_v2" "router_1" {
  name                = "router_1"
  admin_state_up      = "true"
  external_network_id = var.NETWORK.external_network_uuid
}

resource "openstack_networking_router_interface_v2" "interface_1" {
  router_id = "${openstack_networking_router_v2.router_1.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_1.id}"
}

# Create a server
resource "openstack_compute_instance_v2" "instance_1" {
  name        = var.IMAGE_ID.name
  flavor_name = var.IMAGE_ID.flavor_name
  key_pair    = var.SSH_KEY.name
  tags        = []

  security_groups = [
    openstack_networking_secgroup_v2.secgroup_ssh.name,
    openstack_networking_secgroup_v2.secgroup_http_https.name
  ]

  network {
    name = "${openstack_networking_network_v2.network_1.name}"
  }

  block_device {
    uuid                  = var.IMAGE_ID.image_uuid
    source_type           = "image"
    destination_type      = "volume"
    volume_size           = var.DISK_SIZE
    boot_index            = 0
    delete_on_termination = true
  }
}

resource "openstack_compute_floatingip_associate_v2" "floating_ip_1" {
  floating_ip = var.FLOATING_IP
  instance_id = "${openstack_compute_instance_v2.instance_1.id}"

  # This dummy remote-exec will ensure that by the time we run ansible in the next
  # local-exec provisioner, the machine is already up and responsive
  provisioner "remote-exec" {
    inline = ["uname -a"]

    connection {
      host        = self.floating_ip
      type        = "ssh"
      user        = "${var.IMAGE_ID.username}"
      agent       = true
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook --verbose -u '${var.IMAGE_ID.username}' -i '${self.floating_ip},' 'ansible/instance.yaml'"
  }
}
